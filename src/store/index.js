import Vue from 'vue';
import Vuex from 'vuex';
import * as types from './mutation-types';

Vue.use(Vuex);

//initial state
const state = {
    addedToCart: [],
    allItems: [
      {
        id: 'Product-01',
        name: 'iPhone 11 Pro',
        description: 'Apple iPhone 11 Pro 512GB - GOLD',
        price: 1000,
        image:'https://ik.imagekit.io/demo/img/tr:w-300,h-300/default-image.jpg'
      },
      {
        id: 'Product-02',
        name: 'Samsung S10',
        description: 'Samsung Galaxy S10 128GB Dual SIM Enterprise Edition',
        price: 750,
        image:'https://ik.imagekit.io/demo/img/tr:w-300,h-300/default-image.jpg'
      },
      {
        id: 'Product-03',
        name: 'Google Pixel',
        description: 'Google Pixel 3 64GB Black',
        price: 600,
        image:'https://ik.imagekit.io/demo/img/tr:w-300,h-300/default-image.jpg'
      }
    ]
  }
  
  // getters
  const getters = {
    allProducts: state => state.allItems, // would need action/mutation if data fetched async
    getNumberOfProducts: state => (state.allItems) ? state.allItems.length : 0,
    cartProducts: state => {
      return state.addedToCart.map(({ id, quantity }) => {
        const product = state.allItems.find(prod => prod.id === id)
  
        return {
          name: product.name,
          price: product.price,
          quantity
        }
      })
    }
  }
  
  // actions
  const actions = {
    addToCart({ commit }, product){
      commit(types.ADD_TO_CART, {
        id: product.id
      })
    }
  //   emptyCart:({commit}) => {
  //     commit('addedToCart' , null);
  // }
}

  // mutations
const mutations = {

    [types.ADD_TO_CART] (state, { id }) {
        const record = state.addedToCart.find(prod=> prod.id === id)
  
        if (!record) {
          state.addedToCart.push({
            id,
            quantity: 1
          })
        } else {
          record.quantity++
        }
      }
  }

  export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
});