import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App';
import store from './store';
import Items from './components/Items';
import Cart from './components/Cart';
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(VueRouter);
Vue.use(BootstrapVue);
//Defining and registering routes
export const router = new VueRouter ({
 // mode: 'history',
  routes: [
      {path : '/' , component : Items },
      {path : '/cart' , component : Cart }
      ]
});


new Vue({
  router,
    store,
  render: h => h(App),
}).$mount('#app')
